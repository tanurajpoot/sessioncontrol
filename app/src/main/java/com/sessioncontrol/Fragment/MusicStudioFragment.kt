package com.sessioncontrol.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.sessioncontrol.Adapter.Adapter_Pager_Studio
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentMusicBinding
import com.sessioncontrol.databinding.FragmentMusicStudioBinding


class MusicStudioFragment : BaseFragment() {
    lateinit var binding: FragmentMusicStudioBinding
    var adapter: Adapter_Pager_Studio? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMusicStudioBinding.inflate(layoutInflater, container, false)
        setStatusBar(mFragment, R.color.home_color)

        //click tablayout
        setTab()

        return binding.root
    }

    fun setTab() {

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Pending"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Awaiting"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Confirmed"))
        adapter = Adapter_Pager_Studio(childFragmentManager, binding.tabLayout.getTabCount())
        binding.viewPager.setAdapter(adapter)
        binding.tabLayout.setupWithViewPager(binding.viewPager)

        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

}