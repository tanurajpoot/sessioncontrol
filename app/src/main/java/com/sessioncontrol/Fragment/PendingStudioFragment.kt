package com.sessioncontrol.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sessioncontrol.Adapter.PendingAdapter
import com.sessioncontrol.Adapter.PendingStudioAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentPendingBinding
import com.sessioncontrol.databinding.FragmentPendingStudioBinding


class PendingStudioFragment : BaseFragment() {

    lateinit var binding: FragmentPendingStudioBinding
    var pendingStudioAdapter: PendingStudioAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        binding = FragmentPendingStudioBinding.inflate(layoutInflater, container, false)

        //set adapter
        setAdapter()

        return binding.root
    }

    private fun setAdapter() {
        pendingStudioAdapter = PendingStudioAdapter(requireActivity())
        binding.pendingStudioRV?.adapter = pendingStudioAdapter
    }

}