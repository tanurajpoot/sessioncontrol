package com.sessioncontrol.Fragment

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.sessioncontrol.Adapter.HoursAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentCalnderBinding
import com.suke.widget.SwitchButton
import kotlinx.android.synthetic.main.calender_bottomsheet.*
import kotlinx.android.synthetic.main.item_home.*


class CalnderFragment : BaseFragment() {
    lateinit var binding: FragmentCalnderBinding

    var hoursAdapter: HoursAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCalnderBinding.inflate(layoutInflater, container, false)


        // perform click
        performClicks()

        //set adapter
        setAdapter()

        return binding.root
    }


    fun performClicks() {

        binding.floatingBt.setOnClickListener {
            openCalenderBottomSheet()
        }
    }


    fun openCalenderBottomSheet() {

        val dialog = BottomSheetDialog(requireActivity())
        val view: View =
            LayoutInflater.from(requireActivity()).inflate(R.layout.calender_bottomsheet, null)
        dialog!!.setContentView(view)

        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)

        var blockSessionRB = view.findViewById<RadioButton>(R.id.blockSessionRB)
        var setStudioRB = view.findViewById<RadioButton>(R.id.setStudioRB)
        var setSessionLL = view.findViewById<LinearLayout>(R.id.setSessionLL)
        var blockSessionLL = view.findViewById<LinearLayout>(R.id.blockSessionLL)
        var dateLL = view.findViewById<LinearLayout>(R.id.dateLL)
        var startTimeLL = view.findViewById<LinearLayout>(R.id.startTimeLL)
        var endTimeLL = view.findViewById<LinearLayout>(R.id.endTimeLL)
        var weekDayLL = view.findViewById<LinearLayout>(R.id.weekDayLL)
        var weekLL = view.findViewById<LinearLayout>(R.id.weekLL)

        var btSaveTV = view.findViewById<TextView>(R.id.btSaveTV)
        var showDateTV = view.findViewById<TextView>(R.id.showDateTV)
        var startTimeTV = view.findViewById<TextView>(R.id.startTimeTV)
        var endTimeTV = view.findViewById<TextView>(R.id.endTimeTV)
        var calenderIV = view.findViewById<ImageView>(R.id.calenderIV)
        var startTimeIV = view.findViewById<ImageView>(R.id.startTimeIV)
        var endTimeIV = view.findViewById<ImageView>(R.id.endTimeIV)

        var mondayFromRL = view.findViewById<RelativeLayout>(R.id.mondayFromRL)
        var tuesdayFromRL = view.findViewById<RelativeLayout>(R.id.tuesdayFromRL)
        var wednesdayFromRL = view.findViewById<RelativeLayout>(R.id.wednesdayFromRL)
        var thursdayFromRL = view.findViewById<RelativeLayout>(R.id.thursdayFromRL)
        var fridayFromRL = view.findViewById<RelativeLayout>(R.id.fridayFromRL)
        var saturdayFromRL = view.findViewById<RelativeLayout>(R.id.saturdayFromRL)
        var sundayFromRL = view.findViewById<RelativeLayout>(R.id.sundayFromRL)

        var mondayLL = view.findViewById<LinearLayout>(R.id.mondayLL)
        var tuesdayLL = view.findViewById<LinearLayout>(R.id.tuesdayLL)
        var wednesdayLL = view.findViewById<LinearLayout>(R.id.wednesdayLL)
        var thursdayLL = view.findViewById<LinearLayout>(R.id.thursdayLL)
        var fridayLL = view.findViewById<LinearLayout>(R.id.fridayLL)
        var saturdayLL = view.findViewById<LinearLayout>(R.id.saturdayLL)
        var sundayLL = view.findViewById<LinearLayout>(R.id.sundayLL)

        var weekDayView = view.findViewById<View>(R.id.weekDayView)
        var mondayView = view.findViewById<View>(R.id.mondayView)
        var tuesdayView = view.findViewById<View>(R.id.tuesdayView)
        var wednesdayView = view.findViewById<View>(R.id.wednesdayView)
        var thursdayView = view.findViewById<View>(R.id.thursdayView)
        var fridayView = view.findViewById<View>(R.id.fridayView)
        var saturdayView = view.findViewById<View>(R.id.saturdayView)
        var sundayView = view.findViewById<View>(R.id.sundayView)
        var startTimeView = view.findViewById<View>(R.id.startTimeView)
        var endTimeView = view.findViewById<View>(R.id.endTimeView)
        var dateView = view.findViewById<View>(R.id.dateView)


        var weekRB = view.findViewById<RadioButton>(R.id.weekRB)
        var daysRB = view.findViewById<RadioButton>(R.id.daysRB)

        var switchButtonloction = view.findViewById<SwitchButton>(R.id.switchButtonloction)
        var switchButtonTuesday = view.findViewById<SwitchButton>(R.id.switchButtonTuesday)
        var switchButtonWednesday = view.findViewById<SwitchButton>(R.id.switchButtonWednesday)
        var switchButtonThursday = view.findViewById<SwitchButton>(R.id.switchButtonThursday)
        var switchButtonFriday = view.findViewById<SwitchButton>(R.id.switchButtonFriday)
        var switchButtonSaturday = view.findViewById<SwitchButton>(R.id.switchButtonSaturday)
        var switchButtonSunday = view.findViewById<SwitchButton>(R.id.switchButtonSunday)


        var fromTV = view.findViewById<TextView>(R.id.fromTV)
        var tuesdayFromTV = view.findViewById<TextView>(R.id.tuesdayFromTV)
        var wednesdayFromTV = view.findViewById<TextView>(R.id.wednesdayFromTV)
        var thursdayFromTV = view.findViewById<TextView>(R.id.thursdayFromTV)
        var fridayFromTV = view.findViewById<TextView>(R.id.fridayFromTV)
        var saturdayFromTV = view.findViewById<TextView>(R.id.saturdayFromTV)
        var sundayFromTV = view.findViewById<TextView>(R.id.sundayFromTV)


        var toTV = view.findViewById<TextView>(R.id.toTV)
        var tuesdaytoTV = view.findViewById<TextView>(R.id.tuesdaytoTV)
        var wednesdaytoTV = view.findViewById<TextView>(R.id.wednesdaytoTV)
        var thursdaytoTV = view.findViewById<TextView>(R.id.thursdaytoTV)
        var fridayToTV = view.findViewById<TextView>(R.id.fridayToTV)
        var saturdayToTV = view.findViewById<TextView>(R.id.saturdayToTV)
        var sundayToTV = view.findViewById<TextView>(R.id.sundayToTV)




        blockSessionRB.setChecked(true)
        setStudioRB.setChecked(false)

        blockSessionRB.setOnClickListener(View.OnClickListener {
            blockSessionRB.setChecked(true)
            setStudioRB.setChecked(false)
            dateLL.visibility = View.VISIBLE
            weekDayLL.visibility = View.GONE


            mondayLL.visibility = View.GONE
            mondayFromRL.visibility = View.GONE
            weekDayView.visibility = View.GONE


            tuesdayLL.visibility = View.GONE
            tuesdayFromRL.visibility = View.GONE
            tuesdayView.visibility = View.GONE


            wednesdayLL.visibility = View.GONE
            wednesdayFromRL.visibility = View.GONE
            wednesdayView.visibility = View.GONE


            thursdayLL.visibility = View.GONE
            thursdayFromRL.visibility = View.GONE
            thursdayView.visibility = View.GONE


            fridayLL.visibility = View.GONE
            fridayFromRL.visibility = View.GONE
            fridayView.visibility = View.GONE

            saturdayLL.visibility = View.GONE
            saturdayFromRL.visibility = View.GONE
            saturdayView.visibility = View.GONE


            sundayLL.visibility = View.GONE
            sundayFromRL.visibility = View.GONE
            sundayView.visibility = View.GONE

            startTimeLL.visibility = View.GONE
            endTimeLL.visibility = View.GONE

            startTimeView.visibility = View.GONE
            endTimeView.visibility = View.GONE

        })

        blockSessionLL.setOnClickListener(View.OnClickListener {
            blockSessionRB.setChecked(true)
            setStudioRB.setChecked(false)
            dateLL.visibility = View.VISIBLE
            weekDayLL.visibility = View.GONE

            mondayLL.visibility = View.GONE
            mondayFromRL.visibility = View.GONE
            weekDayView.visibility = View.GONE


            tuesdayLL.visibility = View.GONE
            tuesdayFromRL.visibility = View.GONE
            tuesdayView.visibility = View.GONE


            wednesdayLL.visibility = View.GONE
            wednesdayFromRL.visibility = View.GONE
            wednesdayView.visibility = View.GONE


            thursdayLL.visibility = View.GONE
            thursdayFromRL.visibility = View.GONE
            thursdayView.visibility = View.GONE


            fridayLL.visibility = View.GONE
            fridayFromRL.visibility = View.GONE
            fridayView.visibility = View.GONE

            saturdayLL.visibility = View.GONE
            saturdayFromRL.visibility = View.GONE
            saturdayView.visibility = View.GONE


            sundayLL.visibility = View.GONE
            sundayFromRL.visibility = View.GONE
            sundayView.visibility = View.GONE


            startTimeLL.visibility = View.GONE
            endTimeLL.visibility = View.GONE

            startTimeView.visibility = View.GONE
            endTimeView.visibility = View.GONE

        })

        setStudioRB.setOnClickListener(View.OnClickListener {
            setStudioRB.setChecked(true)
            blockSessionRB.setChecked(false)
            daysRB.setChecked(false)

            weekDayLL.visibility = View.VISIBLE
            weekDayView.visibility = View.VISIBLE
            dateLL.visibility = View.GONE

            startTimeLL.visibility = View.GONE
            endTimeLL.visibility = View.GONE

            startTimeView.visibility = View.GONE
            endTimeView.visibility = View.GONE
            dateView.visibility = View.GONE

        })

        setSessionLL.setOnClickListener(View.OnClickListener {
            setStudioRB.setChecked(true)
            blockSessionRB.setChecked(false)
            daysRB.setChecked(false)

            weekDayLL.visibility = View.VISIBLE
            weekDayView.visibility = View.VISIBLE
            dateLL.visibility = View.GONE

            startTimeLL.visibility = View.GONE
            endTimeLL.visibility = View.GONE

            startTimeView.visibility = View.GONE
            endTimeView.visibility = View.GONE
            dateView.visibility = View.GONE


        })

        weekRB.setOnClickListener(View.OnClickListener {
            weekRB.setChecked(true)
            daysRB.setChecked(false)

            mondayLL.visibility = View.VISIBLE
            tuesdayLL.visibility = View.VISIBLE
            wednesdayLL.visibility = View.VISIBLE
            thursdayLL.visibility = View.VISIBLE
            fridayLL.visibility = View.VISIBLE
            saturdayLL.visibility = View.VISIBLE
            sundayLL.visibility = View.VISIBLE

            weekDayView.visibility = View.VISIBLE
            mondayView.visibility = View.VISIBLE
            tuesdayView.visibility = View.VISIBLE
            wednesdayView.visibility = View.VISIBLE
            thursdayView.visibility = View.VISIBLE
            fridayView.visibility = View.VISIBLE
            saturdayView.visibility = View.VISIBLE
            sundayView.visibility = View.VISIBLE


            startTimeLL.visibility = View.GONE
            endTimeLL.visibility = View.GONE

            startTimeView.visibility = View.GONE
            endTimeView.visibility = View.GONE


        })

        switchButtonloction.setOnCheckedChangeListener { view, isChecked ->

            if (isChecked){
                mondayFromRL.visibility = View.VISIBLE
            } else{
                mondayFromRL.visibility = View.GONE

            }
        }


        switchButtonTuesday.setOnCheckedChangeListener { view, isChecked ->

            if (isChecked){
                tuesdayFromRL.visibility = View.VISIBLE
            } else{
                tuesdayFromRL.visibility = View.GONE
            }
        }

        switchButtonWednesday.setOnCheckedChangeListener { view, isChecked ->

            if (isChecked){
                wednesdayFromRL.visibility = View.VISIBLE
            } else{
                wednesdayFromRL.visibility = View.GONE
            }
        }


        switchButtonThursday.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked){
                thursdayFromRL.visibility = View.VISIBLE
            } else{
                thursdayFromRL.visibility = View.GONE
            }
        }

        switchButtonFriday.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked){
                fridayFromRL.visibility = View.VISIBLE
            } else{
                fridayFromRL.visibility = View.GONE
            }
        }


        switchButtonSaturday.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked){
                saturdayFromRL.visibility = View.VISIBLE
            } else{
                saturdayFromRL.visibility = View.GONE
            }
        }

        switchButtonSunday.setOnCheckedChangeListener { view, isChecked ->
            if (isChecked){
                sundayFromRL.visibility = View.VISIBLE
            } else{
                sundayFromRL.visibility = View.GONE
            }
        }


        daysRB.setOnClickListener(View.OnClickListener {
            daysRB.setChecked(true)
            weekRB.setChecked(false)

            dateLL.visibility = View.VISIBLE
            startTimeLL.visibility = View.VISIBLE
            endTimeLL.visibility = View.VISIBLE

            startTimeView.visibility = View.VISIBLE
            endTimeView.visibility = View.VISIBLE
            weekDayView.visibility = View.VISIBLE
            dateView.visibility = View.VISIBLE


            mondayLL.visibility = View.GONE
            mondayFromRL.visibility = View.GONE
            mondayView.visibility = View.GONE


            tuesdayLL.visibility = View.GONE
            tuesdayFromRL.visibility = View.GONE
            tuesdayView.visibility = View.GONE


            wednesdayLL.visibility = View.GONE
            wednesdayFromRL.visibility = View.GONE
            wednesdayView.visibility = View.GONE


            thursdayLL.visibility = View.GONE
            thursdayFromRL.visibility = View.GONE
            thursdayView.visibility = View.GONE


            fridayLL.visibility = View.GONE
            fridayFromRL.visibility = View.GONE
            fridayView.visibility = View.GONE

            saturdayLL.visibility = View.GONE
            saturdayFromRL.visibility = View.GONE
            saturdayView.visibility = View.GONE


            sundayLL.visibility = View.GONE
            sundayFromRL.visibility = View.GONE
            sundayView.visibility = View.GONE

        })

        btSaveTV.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            binding.clickOnTV.visibility = View.GONE
            binding.hourRV.visibility = View.VISIBLE
            binding.studioHourTV.visibility = View.VISIBLE
        })

        calenderIV.setOnClickListener(View.OnClickListener {
            datePicker(showDateTV, calenderIV)
        })

        startTimeIV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(startTimeTV)
        })

        endTimeIV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(endTimeTV)
        })



        // From and To apply time picker

        fromTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(fromTV)
        })

        tuesdayFromTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(tuesdayFromTV)
        })

        wednesdayFromTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(wednesdayFromTV)
        })

        thursdayFromTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(thursdayFromTV)
        })

        fridayFromTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(fridayFromTV)
        })

        saturdayFromTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(saturdayFromTV)
        })

        sundayFromTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(sundayFromTV)
        })

        // at To apply datepicker

        toTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(toTV)
        })

        tuesdaytoTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(tuesdaytoTV)
        })

        wednesdaytoTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(wednesdaytoTV)
        })

        thursdaytoTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(thursdaytoTV)
        })

        fridayToTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(fridayToTV)
        })

        saturdayToTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(saturdayToTV)
        })

        sundayToTV.setOnClickListener(View.OnClickListener {
            calendarDatePicker(sundayToTV)
        })


        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()
    }

    private fun setAdapter() {
        hoursAdapter = HoursAdapter(requireActivity())
        binding.hourRV?.adapter = hoursAdapter
    }


}