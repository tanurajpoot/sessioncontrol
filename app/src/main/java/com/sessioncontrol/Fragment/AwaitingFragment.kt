package com.sessioncontrol.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sessioncontrol.Adapter.AwaitingAdapter
import com.sessioncontrol.Adapter.PendingAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentAwaitingBinding
import com.sessioncontrol.databinding.FragmentPendingStudioBinding


class AwaitingFragment : Fragment() {

    lateinit var binding: FragmentAwaitingBinding
    var awaitingBinding: AwaitingAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentAwaitingBinding.inflate(layoutInflater, container, false)

        //set adapter
        setAdapter()

        return binding.root
    }

    private fun setAdapter() {
        awaitingBinding = AwaitingAdapter(requireActivity())
        binding.awaitingRV?.adapter = awaitingBinding
    }

}