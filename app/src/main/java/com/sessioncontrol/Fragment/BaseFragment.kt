package com.sessioncontrol.Fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.sessioncontrol.Activity.LoginActivity
import com.sessioncontrol.R
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.lang.StringBuilder
import java.text.Format
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.AM
import java.util.Calendar.PM
import java.util.regex.Pattern


open class BaseFragment : Fragment() {

    open var TAG: String = this@BaseFragment.javaClass.getSimpleName()
    open var mFragment: Fragment = this@BaseFragment

    lateinit var mBitmap: Bitmap
    var mCheckCamGal: String = ""


    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = android.Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = android.Manifest.permission.CAMERA


    //to switch between fragments*
    open fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager: FragmentManager = childFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayout, fragment, Tag)
            fragmentManager.popBackStack()
            fragmentTransaction.addToBackStack(null).commit();

            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()

        }
    }

    open fun replaceFragment(fragment: Fragment, Tag: String, addToStack: Boolean) {
        val fragmentTransaction = fragmentManager?.beginTransaction()
        fragmentTransaction?.replace(R.id.frameLayout, fragment, Tag)
        fragmentManager?.popBackStack();
        fragmentTransaction?.commit()
        fragmentManager?.executePendingTransactions()
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun setStatusBar(mFragment: Fragment, mColor: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            requireActivity().window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }


    // - - Sets Appointment Date
    open fun datePicker(mTextView: TextView, mImageView: ImageView) {
        val text: TextView? = null
        val sdf = SimpleDateFormat("EEEE, MMM dd, yyyy", Locale.getDefault())
        val mycalender = Calendar.getInstance()
        val date =
            DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
                mycalender[Calendar.YEAR] = year
                mycalender[Calendar.MONTH] = month
                mycalender[Calendar.DAY_OF_MONTH] = dayOfMonth
                mTextView.setText(sdf.format(mycalender.time))
            }
        mTextView.setOnClickListener(View.OnClickListener { view: View? ->
            DatePickerDialog(
                requireActivity(), date,
                mycalender[Calendar.YEAR],
                mycalender[Calendar.MONTH],
                mycalender[Calendar.DAY_OF_MONTH]
            ).show()
        })

        mImageView.setOnClickListener(View.OnClickListener { view: View? ->
            DatePickerDialog(
                requireActivity(), date,
                mycalender[Calendar.YEAR],
                mycalender[Calendar.MONTH],
                mycalender[Calendar.DAY_OF_MONTH]
            ).show()
        })
    }


    // - - Sets Appointment Date
    open fun calendarDatePicker(mTextView: TextView) {
        var am_pm: String
        val stringBuilder = StringBuilder()


        try {
         /*   var time = "06:06:30"
            val sdf = SimpleDateFormat("hh:mm aa", Locale.getDefault())
            val dt = sdf.parse(time)
            val sdfs = SimpleDateFormat("hh:mm a")
            val formatedTime = sdfs.format(dt)*/

            val mcurrentTime = Calendar.getInstance()
            val hour = mcurrentTime[Calendar.HOUR_OF_DAY]
            val minute = mcurrentTime[Calendar.MINUTE]

            am_pm = getTime(hour,minute).toString()

            stringBuilder.append(" ")
            stringBuilder.append(am_pm)
            mTextView.setText(stringBuilder)

            val mTimePicker: TimePickerDialog
            mTimePicker = TimePickerDialog(
                requireContext(),
                { timePicker, selectedHour, selectedMinute ->
                    mTextView.setText("$selectedHour:$selectedMinute")
                },
                hour,
                minute,

                false
            ) //Yes 24 hour time
            

            mTimePicker.setTitle("Select Time")
            mTimePicker.show()

        } catch (e: ParseException) {
            e.printStackTrace()
        }

    }

    open fun getTime(hr: Int, min: Int): String? {
        val cal = Calendar.getInstance()
        cal[Calendar.HOUR_OF_DAY] = hr
        cal[Calendar.MINUTE] = min
        val formatter: Format
        formatter = SimpleDateFormat("hh:mm aa")
        return formatter.format(cal.time)
    }

        // - - To Show or Hide Password
        open fun setPasswordHideShow(editText: EditText, imageView: ImageView) {
            if (editText.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
                imageView.setImageResource(R.drawable.ic_eye_hide)
                editText.transformationMethod = HideReturnsTransformationMethod.getInstance()
                editText.setSelection(editText.text.toString().trim().length)
            } else {
                imageView.setImageResource(R.drawable.ic_eye_show);
                editText.transformationMethod = PasswordTransformationMethod.getInstance()
                editText.setSelection(editText.text.toString().trim().length)
            }
        }


        // alert dialog pop
        open fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
            val alertDialog = Dialog(mActivity!!)
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog.setContentView(R.layout.dialoge_alert)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            // set the custom dialog components - text, image and button
            val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
            val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
            txtMessageTV.text = strMessage
            btnDismiss.setOnClickListener { alertDialog.dismiss() }
            alertDialog.show()
        }

        /*
    * Validate Email Address
    * */
        open fun isValidEmaillId(email: String?): Boolean {
            return Pattern.compile(
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
            ).matcher(email).matches()
        }

        //alert dialog for logout
        open fun showAlertDialogLogout(mActivity: Activity?, strMessage: String?) {
            val alertDialog = Dialog(mActivity!!)
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog.setContentView(R.layout.alert_dialog_logout)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            // set the custom dialog components - text, image and button
            val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
            val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)
            val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
            txtMessageTV.text = strMessage
            btnNo.setOnClickListener { alertDialog.dismiss() }
            alertDialog.show()

            btnYes.setOnClickListener {
                val intent = Intent(requireActivity(), LoginActivity::class.java)
                startActivity(intent)
                requireActivity().finish()
                alertDialog.dismiss()

            }

        }

        fun openCameraGalleryBottomSheet() {

            val dialog = BottomSheetDialog(requireActivity())
            val view: View =
                LayoutInflater.from(requireActivity()).inflate(R.layout.bottomsheet_carema, null)
            dialog!!.setContentView(view)

            (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
            (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
            (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)

            var takePhotoTv = view.findViewById<TextView>(R.id.takePhotoTV)
            //  var caremaTV = view.findViewById<TextView>(R.id.cameraTv)
            var PhotoLibraryTV = view.findViewById<TextView>(R.id.photoLibraryTV)
            var btCancelTV = view.findViewById<TextView>(R.id.btCancelTV)

            takePhotoTv?.setOnClickListener(View.OnClickListener {

                mCheckCamGal = "camera"
                if (checkPermission()) {
                    onSelectCameraClick()
                } else {
                    requestPermission()
                }
                dialog.dismiss()
            })

            /*   caremaTV?.setOnClickListener(View.OnClickListener {
                   mCheckCamGal = "camera"
                   if (checkPermission()) {
                       onSelectCameraClick()
                   } else {
                       requestPermission()
                   }
                   dialog.dismiss()
               })*/


            PhotoLibraryTV?.setOnClickListener(View.OnClickListener {
                mCheckCamGal = "gallery"
                if (checkPermission()) {
                    onSelectImageClick()
                } else {
                    requestPermission()
                }
                dialog.dismiss()
            })

            btCancelTV.setOnClickListener(View.OnClickListener {
                dialog.dismiss()
            })


            dialog.setCancelable(true)
            dialog.setContentView(view)
            dialog.show()

        }

        // - - To Check permissions
        private fun checkPermission(): Boolean {
            val write = ContextCompat.checkSelfPermission(requireActivity(), writeExternalStorage)
            val read = ContextCompat.checkSelfPermission(requireActivity(), writeReadStorage)
            val camera = ContextCompat.checkSelfPermission(requireActivity(), writeCamera)
            return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
        }

        // - - To Request permissions
        private fun requestPermission() {
            ActivityCompat.requestPermissions(
                requireActivity(), arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
            )
        }

        // - - After Request permissions
        override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
        ) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            when (requestCode) {
                369 -> {
                    if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        Log.e(TAG, "Permission has been denied by user")
                    } else {
                        if (mCheckCamGal.equals("gallery")) {
                            onSelectImageClick()

                        } else if (mCheckCamGal.equals("camera")) {
                            onSelectCameraClick()
                        }
                    }
                }
            }
        }

        // - - Start Image Picker Activity
        private fun onSelectImageClick() {
            ImagePicker.with(this)
                //Crop image(Optional), Check Customization for more option
                .crop()
                .galleryOnly()

                //Final image size will be less than 1 MB(Optional)
                .compress(120)

                //Final image resolution will be less than 1080 x 1080(Optional)
                .maxResultSize(200, 200)
                .cropSquare()
                .start()
        }


        // - - Start Image Picker Activity
        private fun onSelectCameraClick() {
            ImagePicker.with(this)
                //Crop image(Optional), Check Customization for more option
                .crop()
                .cameraOnly()

                //Final image size will be less than 1 MB(Optional)
                .compress(120)

                //Final image resolution will be less than 1080 x 1080(Optional)
                .maxResultSize(200, 200)
                .cropSquare()
                .start()
        }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)

            when (resultCode) {
                AppCompatActivity.RESULT_OK -> {
                    try {
                        //Image Uri will not be null for RESULT_OK
                        val uri: Uri = data?.data!!

                        // Use Uri object instead of File to avoid storage permissions
                        Glide
                            .with(this)
                            .load(uri)
                            .centerCrop()
                            .placeholder(R.drawable.ic_placeholder)
                        // .into(binding.addCameraGallery)

                        val imageStream = requireActivity().contentResolver.openInputStream(uri)
                        val selectedImage = BitmapFactory.decodeStream(imageStream)
                        val out = ByteArrayOutputStream()
                        selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                        mBitmap = selectedImage
                    } catch (e: IOException) {
                        Log.e(TAG, "****Error****" + e.printStackTrace())
                        e.printStackTrace()
                    }

                }
                ImagePicker.RESULT_ERROR -> {
                    Toast.makeText(
                        requireActivity(),
                        ImagePicker.getError(data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }