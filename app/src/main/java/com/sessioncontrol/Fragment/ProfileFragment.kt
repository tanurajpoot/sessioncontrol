package com.sessioncontrol.Fragment

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.sessioncontrol.Activity.*
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentProfileBinding
import java.io.ByteArrayOutputStream
import java.io.IOException

class ProfileFragment : BaseFragment() {
    lateinit var binding: FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)

        // perform click
        performClicks()

        // set backpress
       // onBackPress()

        return binding.root
    }


    fun performClicks() {

        binding.accountInfoRL.setOnClickListener {
            var intent = Intent(requireActivity(), AccountInfoActivity::class.java)
            startActivity(intent)
        }

        binding.changePasswordRL.setOnClickListener {
            var intent = Intent(requireActivity(), ChangePasswordActivity::class.java)
            startActivity(intent)
        }

        binding.editRL.setOnClickListener {
            var intent = Intent(requireActivity(), EditActivity::class.java)
            startActivity(intent)
        }

        binding.paymentMethodRL.setOnClickListener {
            var intent = Intent(requireActivity(), PaymentMethodActivity::class.java)
            startActivity(intent)
        }


        binding.favoriteStudioRL.setOnClickListener {
            var intent = Intent(requireActivity(), FavoriteActivity::class.java)
            startActivity(intent)
        }

        binding.termConditionRL.setOnClickListener {
            var intent = Intent(requireActivity(), TermConditionActivity::class.java)
            startActivity(intent)
        }

        binding.privacyPolicyRL.setOnClickListener {
            var intent = Intent(requireActivity(), PrivacyPolicyActivity::class.java)
            startActivity(intent)
        }


        binding.cancellationRL.setOnClickListener {
            var intent = Intent(requireActivity(), CancellationPolicyActivity::class.java)
            startActivity(intent)
        }

        binding.signupAsRL.setOnClickListener {
            var intent = Intent(requireActivity(), SignUpAsStudioActivity::class.java)
            startActivity(intent)
            //requireActivity().finish()
        }


        binding.logoutRL.setOnClickListener {
            showAlertDialogLogout(requireActivity(), getString(R.string.are_you_logout))
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            AppCompatActivity.RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!

                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .into(binding.profilIV)

                    val imageStream = requireActivity().contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }

            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(requireActivity(), ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }

  /*  fun onBackPress() {
        activity?.onBackPressedDispatcher?.addCallback(requireActivity(), object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // in here you can do logic when backPress is clicked

            }
        })
    }*/
}