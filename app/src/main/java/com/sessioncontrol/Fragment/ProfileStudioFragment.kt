package com.sessioncontrol.Fragment

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.sessioncontrol.Activity.*
import com.sessioncontrol.Adapter.ImagesUploadAdapter
import com.sessioncontrol.MainActivity
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentProfileStudioBinding

import java.io.ByteArrayOutputStream
import java.io.IOException


class ProfileStudioFragment : BaseFragment() {

    lateinit var binding: FragmentProfileStudioBinding
    var imagesUploadAdapter: ImagesUploadAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentProfileStudioBinding.inflate(layoutInflater, container, false)

        // perform click
        performClicks()

        //set adapter
        setAdapter()

        return binding.root
    }

    fun performClicks() {

        binding.accountInfoRL.setOnClickListener {
            var intent = Intent(requireActivity(), AccountInfoStudioActivity::class.java)
            startActivity(intent)
        }

        binding.feedbackRL.setOnClickListener {
            var intent = Intent(requireActivity(), SubmitFeedBackActivity::class.java)
            startActivity(intent)
        }

        binding.editRL.setOnClickListener {
            var intent = Intent(requireActivity(), EditStudioActivity::class.java)
            startActivity(intent)
        }

        binding.termConditionRL.setOnClickListener {
            var intent = Intent(requireActivity(), TermConditionActivity::class.java)
            startActivity(intent)
        }

        binding.privacyPolicyRL.setOnClickListener {
            var intent = Intent(requireActivity(), PrivacyPolicyActivity::class.java)
            startActivity(intent)
        }

        binding.cancellationRL.setOnClickListener {
            var intent = Intent(requireActivity(), CancellationPolicyActivity::class.java)
            startActivity(intent)
        }

        binding.switchToArtistRL.setOnClickListener {
            var intent = Intent(requireActivity(), MainActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }

        binding.paymentMethodRL.setOnClickListener {
            var intent = Intent(requireActivity(), PaymentMethodStudioActivity::class.java)
            startActivity(intent)
        }

        binding.logoutRL.setOnClickListener {
            showAlertDialogLogout(requireActivity(), getString(R.string.are_you_logout))
        }
    }

    private fun setAdapter() {
        imagesUploadAdapter = ImagesUploadAdapter(requireActivity())
        binding.imagesUploadRV?.adapter = imagesUploadAdapter
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            AppCompatActivity.RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!

                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .into(binding.profilIV)

                    val imageStream = requireActivity().contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }

            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(requireActivity(), ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }
}