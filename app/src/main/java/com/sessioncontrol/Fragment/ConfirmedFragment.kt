package com.sessioncontrol.Fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sessioncontrol.Adapter.ConfirmedAdapter
import com.sessioncontrol.Adapter.PendingAdapter
import com.sessioncontrol.databinding.FragmentConfirmedBinding
import com.sessioncontrol.databinding.FragmentPendingBinding


class ConfirmedFragment : BaseFragment() {
    lateinit var binding: FragmentConfirmedBinding
    var confirmedAdapter: ConfirmedAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
            binding = FragmentConfirmedBinding.inflate(layoutInflater, container, false)

            setAdapter()

            return binding.root

    }

    private fun setAdapter() {
        confirmedAdapter = ConfirmedAdapter(requireActivity())
        binding.confirmedRV?.adapter = confirmedAdapter
    }


}