package com.sessioncontrol.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sessioncontrol.Adapter.ConfirmedAdapter
import com.sessioncontrol.Adapter.MessagesAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentConfirmedBinding
import com.sessioncontrol.databinding.FragmentMessageBinding


class MessageFragment : BaseFragment() {
    lateinit var binding: FragmentMessageBinding
    var messagesAdapter: MessagesAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentMessageBinding.inflate(layoutInflater, container, false)
        setStatusBar(mFragment, R.color.home_color)

        setAdapter()

        return binding.root
    }

    private fun setAdapter() {
        messagesAdapter = MessagesAdapter(requireActivity())
        binding.messageRV?.adapter = messagesAdapter
    }

}