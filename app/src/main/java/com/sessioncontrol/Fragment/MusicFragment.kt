package com.sessioncontrol.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.sessioncontrol.Adapter.Adapter_Pager
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentMusicBinding


class MusicFragment : BaseFragment() {

    lateinit var binding: FragmentMusicBinding
    var adapter: Adapter_Pager? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMusicBinding.inflate(layoutInflater, container, false)
        setStatusBar(mFragment, R.color.home_color)

        setTab()

        return binding.root
    }

    fun setTab() {

        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Pending"))
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Confirmed"))
        adapter = Adapter_Pager(childFragmentManager, binding.tabLayout.getTabCount())
        binding.viewPager.setAdapter(adapter)
        binding.tabLayout.setupWithViewPager(binding.viewPager)

        binding.viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {}
            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

}