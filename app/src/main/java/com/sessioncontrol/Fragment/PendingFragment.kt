package com.sessioncontrol.Fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sessioncontrol.Adapter.PendingAdapter
import com.sessioncontrol.databinding.FragmentPendingBinding


class PendingFragment : BaseFragment() {

    lateinit var binding: FragmentPendingBinding
    var pendingAdapter: PendingAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentPendingBinding.inflate(layoutInflater, container, false)

        setAdapter()

        return binding.root
    }

    private fun setAdapter() {
        pendingAdapter = PendingAdapter(requireActivity())
        binding.pendingRV?.adapter = pendingAdapter
    }

}