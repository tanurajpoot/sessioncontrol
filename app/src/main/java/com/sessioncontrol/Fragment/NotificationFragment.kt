package com.sessioncontrol.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sessioncontrol.Adapter.MessagesAdapter
import com.sessioncontrol.Adapter.NotificationAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.FragmentMessageBinding
import com.sessioncontrol.databinding.FragmentNotificationBinding


class NotificationFragment : BaseFragment() {

    lateinit var binding: FragmentNotificationBinding
    var notificationAdapter: NotificationAdapter? = null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationBinding.inflate(layoutInflater, container, false)
        setStatusBar(mFragment, R.color.home_color)

        setAdapter()
        return binding.root
    }

    private fun setAdapter() {
        notificationAdapter = NotificationAdapter(requireActivity())
        binding.notificationRV?.adapter = notificationAdapter
    }


}