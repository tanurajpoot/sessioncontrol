package com.sessioncontrol.Adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.Activity.MessageItemUi
import com.sessioncontrol.R

class ChatAdapter(val mActivity: Activity, var data: MutableList<MessageItemUi>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var VIEW_TYPE_ONE = 0
    var VIEW_TYPE_TWO = 1

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): RecyclerView.ViewHolder {
        val context = parent.context
/*
        if (viewType == VIEW_TYPE_ONE) {
            Log.e("tAG","VALUE::"+"HELLO")
            val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_send, parent, false)
            return SendChat(view)
        }
            return ReceiveChat(LayoutInflater.from(mActivity).inflate(R.layout.item_receive_chat, parent, false))*/

        return when (viewType) {
            VIEW_TYPE_ONE -> {
                val view = LayoutInflater.from(context).inflate(R.layout.item_send, parent, false)
                SendChat(view)
            }
            VIEW_TYPE_TWO -> {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_receive_chat, parent, false)
                ReceiveChat(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }

    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemViewType(position: Int): Int {
        val message: MessageItemUi = data.get(position) as MessageItemUi

        if (message.messageType == 0) {
          return  VIEW_TYPE_ONE

        } else if (message.messageType == 1) {
          return  VIEW_TYPE_TWO
        }
        return -1

    }


    class SendChat(val view: View) : RecyclerView.ViewHolder(view) {
        private val messageText = view.findViewById<TextView>(R.id.sendChatTV)

        fun bind(message: MessageItemUi) {
            messageText.text = message.text
        }
    }

    //apply,lazy,m/scoped

    open inner class ReceiveChat(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var receiveChatTV = itemView.findViewById<TextView>(R.id.receiveChatTV)

        fun bind(message: MessageItemUi) {
            receiveChatTV.text = message.text
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]

        when (holder) {
            is SendChat -> holder.bind(item)
            is ReceiveChat -> holder.bind(item)
            else -> throw IllegalArgumentException()
        }
    }
}