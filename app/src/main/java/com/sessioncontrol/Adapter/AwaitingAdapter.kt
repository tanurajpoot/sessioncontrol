package com.sessioncontrol.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.R


    class AwaitingAdapter(val mActivity: FragmentActivity) :
        RecyclerView.Adapter<AwaitingAdapter.ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): AwaitingAdapter.ViewHolder {
            val view: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_awaiting, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: AwaitingAdapter.ViewHolder, position: Int) {


        }

        override fun getItemCount(): Int {
            return 1
        }

        open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        }

    }