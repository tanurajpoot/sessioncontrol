package com.sessioncontrol.Adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.sessioncontrol.Fragment.ConfirmedFragment
import com.sessioncontrol.Fragment.PendingFragment

class Adapter_Pager(fm: FragmentManager?, var tabC: Int) : FragmentPagerAdapter(fm!!) {
    lateinit var context: Context

    override fun getItem(position: Int): Fragment {
        return when (position) {

            0 -> PendingFragment()
            1 -> ConfirmedFragment()
            else -> PendingFragment()
        }
    }

    override fun getCount(): Int {
        return tabC
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0)
            title = "Pending"
        else if (position == 1)
            title = "Confirmed"
        return title
    }


}
