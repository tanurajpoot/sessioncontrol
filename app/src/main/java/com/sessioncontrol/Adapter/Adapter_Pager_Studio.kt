package com.sessioncontrol.Adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.sessioncontrol.Fragment.AwaitingFragment
import com.sessioncontrol.Fragment.ConfirmedFragment
import com.sessioncontrol.Fragment.PendingFragment
import com.sessioncontrol.Fragment.PendingStudioFragment

class Adapter_Pager_Studio(fm: FragmentManager?, var tabC: Int) : FragmentPagerAdapter(fm!!) {

    lateinit var context: Context

    override fun getItem(position: Int): Fragment {
        return when (position) {

            0 -> PendingStudioFragment()
            1 -> AwaitingFragment()
            2 -> AwaitingFragment()
            else -> PendingStudioFragment()
        }
    }

    override fun getCount(): Int {
        return tabC
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title: String? = null
        if (position == 0)
            title = "Pending"
        else if (position == 1)
            title = "Awaiting"
        else if (position==2)
            title ="Confirmed"
        return title
    }
}