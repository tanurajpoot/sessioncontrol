package com.sessioncontrol.Adapter

import android.content.Intent
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.sessioncontrol.Activity.MapActivity
import com.sessioncontrol.Activity.MapImageActivity
import com.sessioncontrol.R
import com.viewpagerindicator.CirclePageIndicator
import java.util.*

class HomeAdapter(val mActivity: FragmentActivity) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    private val urls = arrayOf(R.drawable.header_home_img,R.drawable.ic_splash_img_bg,R.drawable.ic_video_img)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeAdapter.ViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_home, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: HomeAdapter.ViewHolder, position: Int) {

           holder.viewpager!!.adapter = SlidingImage_Adapter(mActivity, urls)
           holder.indicator.setViewPager(holder.viewpager)

     /*       val density = res.displayMetrics.density
            Set circle indicator radius
           holder.indicator.radius = 5 * density*/

            NUM_PAGES = urls.size

            // Auto start of viewpager
            val handler = Handler()
            val Update = Runnable {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0
                }
                holder.viewpager!!.setCurrentItem(currentPage++, true)
            }
            val swipeTimer = Timer()
            swipeTimer.schedule(object : TimerTask() {
                override fun run() {
                    handler.post(Update)
                }
            }, 3000, 3000)

            // Pager listener over indicator
           holder.indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageSelected(position: Int) {
                    currentPage = position


                }
                override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {
                }
                override fun onPageScrollStateChanged(pos: Int) {

                }
            })

        holder.roundedIV.setOnClickListener {
            val intent = Intent(mActivity,MapImageActivity::class.java)
            mActivity.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return 1
    }

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         var viewpager: ViewPager
         var indicator: CirclePageIndicator
         var roundedIV: ImageView


        init {
            viewpager = itemView.findViewById<View>(R.id.viewpager) as ViewPager
            indicator = itemView.findViewById<View>(R.id.indicator) as CirclePageIndicator
            roundedIV = itemView.findViewById<View>(R.id.roundedIV) as ImageView

        }
    }

    companion object {

        var currentPage = 0
        var NUM_PAGES = 0
    }


}