package com.sessioncontrol.Adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.Activity.ChatActivity
import com.sessioncontrol.Activity.SeeMoreActivity
import com.sessioncontrol.R


class MessagesAdapter(val mActivity: FragmentActivity) : RecyclerView.Adapter<MessagesAdapter.ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int): MessagesAdapter.ViewHolder {
            val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_message, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: MessagesAdapter.ViewHolder, position: Int) {

            holder.itemView.setOnClickListener {

              val intent = Intent(mActivity, ChatActivity::class.java)
                mActivity.startActivity(intent)
            }


        }

        override fun getItemCount(): Int {
            return 2
        }

        open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var dateTV: TextView

            init {
                dateTV = itemView.findViewById<View>(R.id.dateTV) as TextView
            }
        }



    }
