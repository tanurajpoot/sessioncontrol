package com.sessioncontrol.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.R

class PendingAdapter(val mActivity: FragmentActivity) :
    RecyclerView.Adapter<PendingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PendingAdapter.ViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_pending, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PendingAdapter.ViewHolder, position: Int) {

        /*    if (position == 1) {
                holder.countryImg.setImageResource(R.drawable.ic_us_png)
                holder.countryDollerTV.text = "$ 345.67"
                holder.countryNameTV.text = "US Doller"
            }*/
        holder.moreInfoTV.setOnClickListener {
            holder.moreInfoTV.visibility=View.GONE
            holder.crossRL.visibility=View.VISIBLE
            holder.cancelTV.visibility=View.VISIBLE
            holder.textTV.visibility=View.VISIBLE
        }

        holder.crossRL.setOnClickListener {
            holder.moreInfoTV.visibility=View.VISIBLE
            holder.crossRL.visibility=View.GONE
            holder.cancelTV.visibility=View.GONE
            holder.textTV.visibility=View.GONE
        }

        holder.cancelTV.setOnClickListener {
            holder.moreInfoTV.visibility=View.VISIBLE
            holder.crossRL.visibility=View.GONE
            holder.cancelTV.visibility=View.GONE
            holder.textTV.visibility=View.GONE
        }
    }

    override fun getItemCount(): Int {
        return 2
    }

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var moreInfoTV: TextView
        var cancelTV: TextView
        var textTV: TextView
        var crossRL: RelativeLayout

        init {
            moreInfoTV = itemView.findViewById<View>(R.id.moreInfoTV) as TextView
            cancelTV = itemView.findViewById<View>(R.id.cancelTV) as TextView
            textTV = itemView.findViewById<View>(R.id.textTV) as TextView
            crossRL = itemView.findViewById<View>(R.id.crossRL) as RelativeLayout

        }
    }

}
