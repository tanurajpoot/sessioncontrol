package com.sessioncontrol.Adapter

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.R

class PendingStudioAdapter(val mActivity: FragmentActivity) :
    RecyclerView.Adapter<PendingStudioAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PendingStudioAdapter.ViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_pending_studio, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PendingStudioAdapter.ViewHolder, position: Int) {


        holder.moreInfoTV.setOnClickListener {
            holder.acceptRejectLL.isVisible = true
            holder.crossRL.isVisible = true
            holder.textTV.isVisible = true
            holder.moreInfoTV.isVisible = false

        }

        holder.crossRL.setOnClickListener {
            holder.acceptRejectLL.isVisible = false
            holder.crossRL.isVisible = false
            holder.textTV.isVisible = false
            holder.moreInfoTV.isVisible = true

        }

        holder.rejectTV.setOnClickListener {
            showRejectAlertDialog()

        }


    }

    override fun getItemCount(): Int {
        return 2
    }

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rejectTV: TextView
        var acceptTV: TextView
        var moreInfoTV: TextView
        var textTV: TextView
        var crossRL: RelativeLayout
        var acceptRejectLL: LinearLayout


        init {
            rejectTV = itemView.findViewById<View>(R.id.rejectTV) as TextView
            acceptTV = itemView.findViewById<View>(R.id.acceptTV) as TextView
            moreInfoTV = itemView.findViewById<View>(R.id.moreInfoTV) as TextView
            textTV = itemView.findViewById<View>(R.id.textTV) as TextView
            crossRL = itemView.findViewById<View>(R.id.crossRL) as RelativeLayout
            acceptRejectLL = itemView.findViewById<View>(R.id.acceptRejectLL) as LinearLayout


        }
    }
    // alert dialog pop
    open fun showRejectAlertDialog() {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_reject_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set the custom dialog components - text, image and button
        val BTrejectTV = alertDialog.findViewById<TextView>(R.id.BTrejectTV)
        val BTcancelTV = alertDialog.findViewById<TextView>(R.id.BTcancelTV)
        BTrejectTV.setOnClickListener { alertDialog.dismiss() }
        BTcancelTV.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }


}
