package com.sessioncontrol.Adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.Activity.SeeMoreActivity
import com.sessioncontrol.R


    class ConfirmedAdapter(val mActivity: FragmentActivity) :
        RecyclerView.Adapter<ConfirmedAdapter.ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ConfirmedAdapter.ViewHolder {
            val view: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_confirmed, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ConfirmedAdapter.ViewHolder, position: Int) {

            holder.seeMoreTV.setOnClickListener {

                val intent = Intent(mActivity,SeeMoreActivity::class.java)
                mActivity.startActivity(intent)
            }


        }

        override fun getItemCount(): Int {
            return 1
        }

        open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
              var seeMoreTV: TextView

            init {
                seeMoreTV = itemView.findViewById<View>(R.id.seeMoreTV) as TextView

            }
        }


}