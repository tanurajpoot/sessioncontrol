package com.sessioncontrol.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.R


class NotificationAdapter(val mActivity: FragmentActivity) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int): NotificationAdapter.ViewHolder {
            val view: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_notification, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: NotificationAdapter.ViewHolder, position: Int) {

            holder.itemView.setOnClickListener {

            }

        }

        override fun getItemCount(): Int {
            return 2
        }

        open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            // var seeMoreTV: TextView

            init {
                //  seeMoreTV = itemView.findViewById<View>(R.id.seeMoreTV) as TextView
            }
        }




}