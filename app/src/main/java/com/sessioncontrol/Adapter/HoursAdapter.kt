package com.sessioncontrol.Adapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.R

    class HoursAdapter(val mActivity: FragmentActivity) :
        RecyclerView.Adapter<HoursAdapter.ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): HoursAdapter.ViewHolder {
            val view: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_hours, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: HoursAdapter.ViewHolder, position: Int) {

        }

        override fun getItemCount(): Int {
            return 2
        }

        open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        }
    }
