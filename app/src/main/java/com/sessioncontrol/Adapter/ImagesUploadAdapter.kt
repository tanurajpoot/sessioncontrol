package com.sessioncontrol.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.sessioncontrol.R


    class ImagesUploadAdapter(val mActivity: FragmentActivity) : RecyclerView.Adapter<ImagesUploadAdapter.ViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int): ImagesUploadAdapter.ViewHolder {
            val view: View =
                LayoutInflater.from(mActivity).inflate(R.layout.item_images, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ImagesUploadAdapter.ViewHolder, position: Int) {


        }

        override fun getItemCount(): Int {
            return 2
        }

        open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            // var seeMoreTV: TextView

            init {
                //  seeMoreTV = itemView.findViewById<View>(R.id.seeMoreTV) as TextView
            }
        }

}