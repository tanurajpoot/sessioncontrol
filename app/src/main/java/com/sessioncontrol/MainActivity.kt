package com.sessioncontrol

import android.graphics.Color
import android.os.BaseBundle
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.navigation.NavigationBarView
import com.sessioncontrol.Activity.BaseActivity
import com.sessioncontrol.Fragment.*
import com.sessioncontrol.databinding.ActivityLoginBinding
import com.sessioncontrol.databinding.ActivityMainBinding

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)


        switchFragment(HomeFragment(),  false, null)

        binding.navigationId.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener {

            when(it.itemId){
                R.id.actionHome -> {
                    switchFragment(HomeFragment(),  false, null)
                    false
                }
                R.id.actionMusic ->{
                    switchFragment(MusicFragment(), false, null)
                    false
                }
                R.id.actionMessage ->{
                    switchFragment(MessageFragment(), false, null)
                    false
                }
                R.id.actionNotification ->{
                    switchFragment(NotificationFragment(), false, null)
                    false
                }
                R.id.actionProfile ->{
                    switchFragment(ProfileFragment(), false, null)
                    false
                }
            }

            true
        })

    }

    override fun onBackPressed() {
        if (fragmentManager.backStackEntryCount == 0) {
            finish()
        } else {
            fragmentManager.popBackStack()
        }
    }



}