package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import com.sessioncontrol.MainActivity
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityForgotPasswordBinding

class ForgotPasswordActivity : BaseActivity() {
    lateinit var binding: ActivityForgotPasswordBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        performClick()

    }

    fun performClick() {

        binding.backRL.setOnClickListener(View.OnClickListener {
                onBackPressed()

        })

        binding.btSubmitTV.setOnClickListener(View.OnClickListener {
            if (isValidate()) {
                onBackPressed()
            }
        })

    }




    private fun isValidate(): Boolean {
        var flag = true
        if (binding.emailET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        }else if (!isValidEmaillId(binding?.emailET?.text.toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        }

        return flag
    }

}