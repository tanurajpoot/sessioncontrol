package com.sessioncontrol.Activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.viewpager.widget.ViewPager
import com.sessioncontrol.Adapter.HomeAdapter
import com.sessioncontrol.Adapter.SlidingImageMap_Adapter
import com.sessioncontrol.Adapter.SlidingImage_Adapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityMapBinding
import java.util.*

class MapActivity : BaseActivity() {
 lateinit var binding : ActivityMapBinding

    private var currentPage = 0
    private var NUM_PAGES = 0
    private val urls = arrayOf(R.drawable.header_home_img,R.drawable.ic_splash_img_bg,R.drawable.ic_video_img)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        performCliks()

        setViewPager()

    }

    fun performCliks(){

        binding.backRL.setOnClickListener {
            onBackPressed()
        }
    }


    fun setViewPager(){
        binding.viewpager!!.adapter = SlidingImageMap_Adapter(mActivity, urls)
        binding.indicator.setViewPager(binding.viewpager)

        /*       val density = res.displayMetrics.density
               Set circle indicator radius
              binding.indicator.radius = 5 * density*/

        HomeAdapter.NUM_PAGES = urls.size

        // Auto start of viewpager
        val handler = Handler()
        val Update = Runnable {
            if (HomeAdapter.currentPage == HomeAdapter.NUM_PAGES) {
                HomeAdapter.currentPage = 0
            }
            binding.viewpager!!.setCurrentItem(HomeAdapter.currentPage++, true)
        }
        val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 3000, 3000)

        // Pager listener over indicator
        binding.indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                HomeAdapter.currentPage = position
            }
            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {
            }
            override fun onPageScrollStateChanged(pos: Int) {

            }
        })

    }
}