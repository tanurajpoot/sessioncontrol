package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.sessioncontrol.MainActivity
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityChangePasswordBinding
import com.sessioncontrol.databinding.ActivityLoginBinding

class ChangePasswordActivity : BaseActivity() {
    lateinit var binding: ActivityChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()


        //Bg selector color
        backgroundSelector()
    }

    fun performClick() {

        binding.backRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })


        binding.btSaveTV.setOnClickListener(View.OnClickListener {

            if (isValidate()) {
                onBackPressed()
            }
        })
    }

    fun backgroundSelector() {
        editTextSelector(binding.passwordET, binding.passwordRL, "")
        editTextSelector(binding.confirmPasswordET, binding.confirmPasswordRL, "")
        editTextSelector(binding.newPasswordET, binding.newPasswordRL, "")
    }


    /*
* Set up validations for Sign In fields
* */
    private fun isValidate(): Boolean {
        var flag = true
        if (binding?.passwordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_Old_password))
            flag = false
        } else if (binding?.passwordET?.getText().toString().trim { it <= ' ' }.length < 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit))
            flag = false
        }  else if (binding?.newPasswordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_password))
            flag = false
        } else if (binding?.newPasswordET?.getText().toString().trim { it <= ' ' }.length < 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit))
            flag = false
        }else if (binding?.confirmPasswordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password))
            flag = false
        } else if (binding?.confirmPasswordET?.getText().toString().trim { it <= ' ' }.length < 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit))
            flag = false
        }
        return flag
    }



}