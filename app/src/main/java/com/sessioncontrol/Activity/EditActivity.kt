package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityEditBinding
import java.io.ByteArrayOutputStream
import java.io.IOException

class EditActivity : BaseActivity() {
    lateinit var binding: ActivityEditBinding

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = android.Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = android.Manifest.permission.CAMERA


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditBinding.inflate(layoutInflater)
        binding = ActivityEditBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

        //set spinner
        setSpinner()

        // edit text bg selector color
        backgroundSelector()

        // text touch bg foucs
        // textTouchSelector()
    }

    fun performClick() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }

        binding.btSaveTV.setOnClickListener(View.OnClickListener {

            if (isValidate()) {
                onBackPressed()
            }
        })

        binding.birthDateTV.setOnClickListener {
            datePicker(binding.birthDateTV)
        }

        binding.birthDateIV.setOnClickListener(View.OnClickListener {
            datePicker(binding.birthDateTV)
        })

   /*     binding.openCameraRL.setOnClickListener(View.OnClickListener {
            openCameraGalleryBottomSheet()

        })*/

        binding.camIV.setOnClickListener(View.OnClickListener {
            openCameraGalleryBottomSheet()
        })

        binding.ccp.setOnCountryChangeListener {
            binding.codeTV.text = binding.ccp.selectedCountryCodeWithPlus

        }

    }

    fun backgroundSelector() {
        editTextSelector(binding?.firstNameET!!, binding!!.firstNameRL, "")
        editTextSelector(binding!!.lastNameET, binding!!.lastNameRL, "")
        editTextSelector(binding!!.emailET, binding!!.emailRL, "")
        editTextSelector(binding!!.mobileET, binding!!.mobileRL, "")
    }

    // set spinner
    private fun setSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()
        mSpinnerList.add("Male")
        mSpinnerList.add("Female")
        mSpinnerList.add("Other")

        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.spinnerGender.adapter = adapter

        binding.spinnerGender.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    backgroundSelector()
                }
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    backgroundSelector()
                }
            }

    }

    /*
* Set up validations for Sign In fields
* */
    fun isValidate(): Boolean {
        var flag = true
        if (binding.firstNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (binding.lastNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (binding.emailET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.text.toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (binding.mobileET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_mobile))
            flag = false
        }
        return flag
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!

                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .into(binding.profileIV)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }

            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }

}