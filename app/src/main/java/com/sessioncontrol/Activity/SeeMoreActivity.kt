package com.sessioncontrol.Activity

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityPayPalBinding
import com.sessioncontrol.databinding.ActivitySeeMoreBinding

class SeeMoreActivity : BaseActivity() {

    lateinit var binding: ActivitySeeMoreBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySeeMoreBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //cliks
        performCliks()


    }

    fun performCliks() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }

        binding.btpayNowTV.setOnClickListener {
            openBottomSheet()
        }
    }

    fun openBottomSheet() {

        val dialog = BottomSheetDialog(mActivity)
        val view: View = LayoutInflater.from(this).inflate(R.layout.item_bottom_sheet, null)
        dialog!!.setContentView(view)

        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)

        var stripeTV = view.findViewById<TextView>(R.id.stripeTV)
        var payPalTV = view.findViewById<TextView>(R.id.payPalTV)
        var btCancelTV = view.findViewById<TextView>(R.id.btCancelTV)

        payPalTV?.setOnClickListener(View.OnClickListener {

            dialog.dismiss()
            val intent = Intent(mActivity, PayPalActivity::class.java)
            startActivity(intent)

        })

        btCancelTV.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()

    }

}