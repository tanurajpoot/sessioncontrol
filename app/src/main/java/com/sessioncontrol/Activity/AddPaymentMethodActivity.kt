package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityAddPaymentMethodBinding

class AddPaymentMethodActivity : BaseActivity() {
    lateinit var binding: ActivityAddPaymentMethodBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddPaymentMethodBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

        //set spinner
        setSpinner()
    }

    fun performClick() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }

        binding.btSubmitTV.setOnClickListener(View.OnClickListener {
            if (isValidate()) {
                onBackPressed()
            }
        })

        binding.dateET.setOnClickListener {
            datePicker(binding.dateET)
        }
    }


    // set spinner
    private fun setSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()

        mSpinnerList.add("Select Card Type")
        mSpinnerList.add("VisaCard")
        mSpinnerList.add("MasterCard")
        mSpinnerList.add("Credite Card")


        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.cardTypeSpinner.adapter = adapter

        binding.cardTypeSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    // backgroundSelector()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    // backgroundSelector()
                }
            }

    }

    /*
* Set up validations for Sign In fields
* */
    private fun isValidate(): Boolean {
        var flag = true
        val spinnerItem: String = binding.cardTypeSpinner.getSelectedItem().toString()
        if ( spinnerItem.equals("Select Card Type")) {
            showAlertDialog(mActivity, getString(R.string.please_select_card))
            flag = false
        } else if (binding.cardNumberET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_number))
            flag = false

        }else if (binding.cardNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_name_on))
            flag = false

        } else if (binding.cvvET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_select_cvv))
            flag = false
        } else if (binding.dateET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_select_date))
            flag = false
        }
        return flag
    }
}