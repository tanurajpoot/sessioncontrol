package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.sessioncontrol.databinding.ActivityTermConditionBinding

class TermConditionActivity : BaseActivity() {
    lateinit var binding: ActivityTermConditionBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTermConditionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //setWeb
        initWebView()

        //clicks
        performClick()

    }


    fun performClick() {

        binding.backRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }


    private fun initWebView() {
        val pdfUrl = "https://www.sessioncontrol.co/Terms_And_Conditions.html" //your pdf url

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.loadUrl(pdfUrl)

    }
}