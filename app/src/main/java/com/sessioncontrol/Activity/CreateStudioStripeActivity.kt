package com.sessioncontrol.Activity

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityCreateStudioStripeBinding
import com.sessioncontrol.databinding.ActivityStudioInformationBinding

class CreateStudioStripeActivity : BaseActivity() {
    lateinit var binding: ActivityCreateStudioStripeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateStudioStripeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()
    }


    fun performClick() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }
        binding.questionMarkIV.setOnClickListener {
            openHelpBottomSheet()
        }

        binding.addressTV.setOnClickListener {
            val intent = Intent(this,AddressAccountInfoActivity::class.java)
            startActivity(intent)
        }

        binding.uloadTV.setOnClickListener {
            openCameraGalleryBottomSheet()
        }
        binding.uloadBackTV.setOnClickListener {
            openCameraGalleryBottomSheet()
        }
    }

    fun  openHelpBottomSheet(){

        val dialog = BottomSheetDialog(this)
        val view: View =
            LayoutInflater.from(this).inflate(R.layout.help_bottomshett, null)
        dialog!!.setContentView(view)

        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)


        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()

    }

}