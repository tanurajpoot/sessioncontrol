package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import com.sessioncontrol.MainActivity
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityLoginBinding

class LoginActivity : BaseActivity() {

    lateinit var binding: ActivityLoginBinding

    var mCheck: Boolean = false

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

        //Bg selector color
        backgroundSelector()
    }

    fun performClick() {

        binding.btSignInTV.setOnClickListener(View.OnClickListener {

            if (isValidate()) {
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        })

        binding.txtSignUpTV.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
            finish()
        })

        binding.forgotTV.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        })



        binding.fbIV.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        })

        binding?.rememRL?.setOnClickListener(View.OnClickListener {

            if (mCheck) {
                binding!!.rememIv.setImageResource(R.drawable.ic_check)
                mCheck = false
            } else {
                binding!!.rememIv.setImageResource(R.drawable.ic_uncheck)
                mCheck = true
            }
        })
    }

    fun backgroundSelector() {
        editTextSelector(binding.emailET, binding.emailRL, "")
        editTextSelector(binding.passwordET, binding.passwordRL, "")
    }

    /*
 * Set up validations for Sign In fields
 * */
    private fun isValidate(): Boolean {
        var flag = true
        if (binding.emailET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.text.toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (binding?.passwordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        } else if (binding?.passwordET?.getText().toString().trim { it <= ' ' }.length < 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit))
            flag = false
        }
        return flag
    }

}