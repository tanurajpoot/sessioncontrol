package com.sessioncontrol.Activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.sessioncontrol.databinding.ActivityLoginWithStripeBinding


class LoginWithStripeActivity : BaseActivity() {
    lateinit var binding: ActivityLoginWithStripeBinding

    var Check : Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginWithStripeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

        //webview
        initWebView()

    }

    fun performClick() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }

        binding.connectRL.setOnClickListener {
            val intent = Intent(this, MainActivityStudio::class.java)
            startActivity(intent)
             finish()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val pdfUrl = "https://www.sessioncontrol.co/Cancellation_Policy.html" //your pdf url
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.loadUrl(pdfUrl)

    }

}