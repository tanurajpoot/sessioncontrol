package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.sessioncontrol.databinding.ActivitySubmitFeedBackBinding

class SubmitFeedBackActivity : BaseActivity() {
    lateinit var binding: ActivitySubmitFeedBackBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySubmitFeedBackBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

    }

    fun performClick() {
        binding.backRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }
}