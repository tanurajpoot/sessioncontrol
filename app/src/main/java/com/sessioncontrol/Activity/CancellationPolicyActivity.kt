package com.sessioncontrol.Activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityCancellationPolicyBinding
import com.sessioncontrol.databinding.ActivityChangePasswordBinding

class CancellationPolicyActivity : BaseActivity() {
    lateinit var binding: ActivityCancellationPolicyBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCancellationPolicyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

        //setWeb
        initWebView()
    }

    fun performClick() {

        binding.backRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }

    private fun initWebView() {
        val pdfUrl = "https://www.sessioncontrol.co/Cancellation_Policy.html" //your pdf url
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.loadUrl(pdfUrl)


    }
}