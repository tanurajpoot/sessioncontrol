package com.sessioncontrol.Activity

import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.MediaController
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityTermConditionBinding
import com.sessioncontrol.databinding.ActivityVideoBinding

class VideoActivity : BaseActivity() {
    lateinit var binding: ActivityVideoBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        playVideoView()
    }


    fun playVideoView(){

        var videoUrl = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4"

        val uri: Uri = Uri.parse(videoUrl)
        binding.videoView.setVideoURI(uri)
        val mediaController = MediaController(this)
        mediaController.setAnchorView( binding.videoView)
        mediaController.setMediaPlayer( binding.videoView)
        binding.videoView.setMediaController(mediaController)
        binding.videoView.start()

    }
}