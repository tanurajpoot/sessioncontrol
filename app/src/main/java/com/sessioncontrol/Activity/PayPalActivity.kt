package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Bundle
import com.sessioncontrol.databinding.ActivityPayPalBinding

class PayPalActivity : BaseActivity() {

    lateinit var binding: ActivityPayPalBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPayPalBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //cliks
        performCliks()
    }

    fun performCliks() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }
    }
}