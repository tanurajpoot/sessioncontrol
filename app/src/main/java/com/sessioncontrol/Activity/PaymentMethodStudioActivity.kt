package com.sessioncontrol.Activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityAccountInfoStudioBinding
import com.sessioncontrol.databinding.ActivityPaymentMethodBinding
import com.sessioncontrol.databinding.ActivityPaymentMethodStudioBinding

class PaymentMethodStudioActivity : BaseActivity() {

    lateinit var binding: ActivityPaymentMethodStudioBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentMethodStudioBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()
    }



    fun performClick() {
        binding.backRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }
}