package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.sessioncontrol.databinding.ActivityPrivacyPolicyBinding

class PrivacyPolicyActivity : BaseActivity() {
    lateinit var binding: ActivityPrivacyPolicyBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPrivacyPolicyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

        //setWeb
        initWebView()

    }

    fun performClick() {

        binding.backRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }

    private fun initWebView() {
        val pdfUrl = "https://www.sessioncontrol.co/Privacy_Policy.html" //your pdf url

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.loadUrl(pdfUrl)


    }
}