package com.sessioncontrol.Activity

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivitySignUpAsStudioBinding

class SignUpAsStudioActivity : BaseActivity() {
    lateinit var binding: ActivitySignUpAsStudioBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpAsStudioBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //cliks
        performCliks()
    }

    fun performCliks() {
        binding.backRL.setOnClickListener {
            onBackPressed()
        }

        binding.studioInfoLL.setOnClickListener {
            var intent = Intent(mActivity, StudioInformationActivity::class.java)
            startActivity(intent)
        }

        binding.usingStripeLL.setOnClickListener {
            openStripeBottomSheet()
        }
    }


    fun openStripeBottomSheet() {
        val dialog = BottomSheetDialog(this)
        val view: View =
            LayoutInflater.from(this).inflate(R.layout.stripe_bottomsheet, null)
        dialog!!.setContentView(view)

        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)

        var loginWithStripe = view.findViewById<TextView>(R.id.loginWithStripe)
        var usingStripeTV = view.findViewById<TextView>(R.id.usingStripeTV)
        var btCancelTV = view.findViewById<TextView>(R.id.btCancelTV)

        loginWithStripe.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            var intent = Intent(mActivity, LoginWithStripeActivity::class.java)
            startActivity(intent)
        })

        usingStripeTV.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            var intent = Intent(mActivity, CreateStudioStripeActivity::class.java)
            startActivity(intent)

        })


        btCancelTV.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })


        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()

    }


}