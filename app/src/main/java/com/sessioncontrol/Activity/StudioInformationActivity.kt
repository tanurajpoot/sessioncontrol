package com.sessioncontrol.Activity


import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RelativeLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityStudioInformationBinding
import kotlinx.android.synthetic.main.item_home.*
import java.io.ByteArrayOutputStream
import java.io.IOException

class StudioInformationActivity : BaseActivity() {
    lateinit var binding: ActivityStudioInformationBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStudioInformationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

        //set spinner
        setGenresSpinner()
        setmixingSpinner()
        setPriceSpinner()

        // edit text bg selector color
        backgroundSelector()

        // text touch bg foucs
        // textTouchSelector()
    }

    fun performClick() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }

        binding.btNextTV.setOnClickListener(View.OnClickListener {

            if (isValidate()) {
                onBackPressed()
            }
        })

        binding.birthDateTV.setOnClickListener {
            datePicker(binding.birthDateTV)
        }

        binding.birthDateIV.setOnClickListener(View.OnClickListener {
            datePicker(binding.birthDateTV)
        })

        binding.addCameraGallery.setOnClickListener {
            openCameraGalleryBottomSheet()
        }

        binding.studioAddressTV.setOnClickListener {
            val intent = Intent(this, AddressAccountInfoActivity::class.java)
            startActivity(intent)
        }

        binding.ccp.setOnCountryChangeListener {
            binding.codeTV.text = binding.ccp.selectedCountryCodeWithPlus

        }
    }

    private fun textTouchSelector() {
        binding!!.birthDateTV.setOnTouchListener(View.OnTouchListener { v, event ->
            changeBGMethod(binding!!.birthDateTV)
            false
        })


        binding!!.lastNameET.setOnTouchListener(View.OnTouchListener { v, event ->
            changeBGMethod(binding!!.lastNameET)
            false
        })
    }


    private fun changeBGMethod(mView: View) {
        mView.requestFocus()
        binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.firstNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.lastNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.emailRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.mobileRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.mobileRL.setBackgroundResource(R.drawable.bg_outline_white)

        if (mView == binding!!.birthDateTV) {
            binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_yellow)
            datePicker(binding?.birthDateTV!!)
        }
    }


    // your birthday set bg gray
    override fun editTextSelector(mEditText: View, rl: RelativeLayout, tag: String) {
        mEditText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_white)
            binding!!.spinnerGenres.setBackgroundResource(R.drawable.bg_outline_white)
            if (hasFocus) {
                rl.setBackgroundResource(R.drawable.bg_outline_yellow)
            } else {
                rl.setBackgroundResource(R.drawable.bg_outline_white)
            }
        }
    }

    fun backgroundSelector() {
        editTextSelector(binding?.studioNameET!!, binding!!.studioNameRL, "")
        editTextSelector(binding!!.lastNameET, binding!!.firstNameRL, "")
        editTextSelector(binding!!.lastNameET, binding!!.lastNameRL, "")
        editTextSelector(binding!!.studioAddressTV, binding!!.studioAddressRL, "")
        editTextSelector(binding!!.emailET, binding!!.emailRL, "")
        editTextSelector(binding!!.mobileET, binding!!.mobileRL, "")
        editTextSelector(binding!!.studioAddressTV, binding!!.studioAddressRL, "")
        editTextSelector(binding!!.mobileET, binding!!.mobileRL, "")
        editTextSelector(binding!!.equipmentET, binding!!.equipmentRL, "")
    }

    // set spinner
    private fun setGenresSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()
        mSpinnerList.add("Rock")
        mSpinnerList.add("Pop")
        mSpinnerList.add("Other")


        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.spinnerGenres.adapter = adapter

        binding.spinnerGenres.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    backgroundSelector()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    backgroundSelector()
                }
            }

    }

    // set spinner
    private fun setmixingSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()
        mSpinnerList.add("Rock")
        mSpinnerList.add("Pop")
        mSpinnerList.add("Other")


        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.spinnerMastering.adapter = adapter

        binding.spinnerMastering.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    backgroundSelector()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    backgroundSelector()
                }
            }

    }

    // set spinner
    private fun setPriceSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()
        mSpinnerList.add("10")
        mSpinnerList.add("20")
        mSpinnerList.add("30")


        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.spinnerPrice.adapter = adapter

        binding.spinnerPrice.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    backgroundSelector()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    backgroundSelector()
                }
            }

    }


    /*
* Set up validations for Sign In fields
* */
    private fun isValidate(): Boolean {
        var flag = true
        if (binding.studioNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_studio_name))
            flag = false
        } else if (binding.firstNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (binding.lastNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (binding.studioAddressTV.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_address_name))
            flag = false
        } else if (binding.emailET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.text.toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (binding.mobileET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_mobile))
            flag = false
        } else if (binding?.studioCreditET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_studiocredit))
            flag = false
        } else if (binding?.equipmentET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_Equipment_password))
            flag = false
        }
        return flag
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!

                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .into(binding.addCameraGallery)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }

            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }


}


