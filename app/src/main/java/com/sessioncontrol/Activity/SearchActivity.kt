package com.sessioncontrol.Activity

import android.os.Build
import android.os.Bundle
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivitySearchBinding

class SearchActivity : BaseActivity() {

    lateinit var binding: ActivitySearchBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setStatusBar(mActivity, getColor(R.color.home_search_color))
        }

        performCliks()
    }

    fun performCliks() {
        binding.backRL.setOnClickListener {
            onBackPressed()
        }
    }
}