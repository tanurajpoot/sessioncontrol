package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivitySignUpBinding
import java.io.ByteArrayOutputStream
import java.io.IOException

class SignUpActivity : BaseActivity() {
    lateinit var binding: ActivitySignUpBinding
    var mCheck: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()

        //set spinner
        setSpinner()

        // edit text bg selector color
        backgroundSelector()

        // text touch bg foucs
        textTouchSelector()


    }

    fun performClick() {

        binding.btSignUpTV.setOnClickListener(View.OnClickListener {

            if (isValidate()) {
                var intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        })

        binding.txtSignInTV.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        })

        binding.birthDateTV.setOnClickListener {
            datePicker(binding.birthDateTV)
        }

        binding.birthDateIV.setOnClickListener(View.OnClickListener {
            datePicker(binding.birthDateTV)
        })

        /*   binding.forgotTV.setOnClickListener(View.OnClickListener {
               var intent = Intent(this, ForgotPasswordActivity::class.java)
               startActivity(intent)
           })*/


  /*      binding.headerRL.setOnClickListener(View.OnClickListener {
            openCameraGalleryBottomSheet()

        })
*/
        binding.camIV.setOnClickListener(View.OnClickListener {
            openCameraGalleryBottomSheet()
        })
/*
        binding.rememRL.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, TermConditionActivity::class.java)
            startActivity(intent)
        })*/


        binding?.rememRL?.setOnClickListener(View.OnClickListener {

            if (mCheck) {
                binding!!.rememIv.setImageResource(R.drawable.ic_check)
                mCheck = false

                var intent = Intent(this, TermConditionActivity::class.java)
                startActivity(intent)

            } else {
                binding!!.rememIv.setImageResource(R.drawable.ic_uncheck)
                mCheck = true
            }
        })

        binding.ccp.setOnCountryChangeListener {

           binding .codeTV.text= binding.ccp.selectedCountryCodeWithPlus

        }

    }
    private fun textTouchSelector() {
        binding!!.birthDateTV.setOnTouchListener(View.OnTouchListener { v, event ->
            changeBGBirthMethod(binding!!.birthDateTV)
            false
        })

        binding!!.spinnerGender.setOnTouchListener(View.OnTouchListener { v, event ->
            changeBGGenderMethod(binding!!.spinnerGender)
            false
        })

        binding!!.lastNameTV.setOnTouchListener(View.OnTouchListener { v, event ->
            changeBGLastNameMethod(binding!!.lastNameTV)
            false
        })


        binding!!.emailTV.setOnTouchListener(View.OnTouchListener { v, event ->
            changeBGEmailMethod(binding!!.lastNameTV)
            false
        })

    }


    private fun changeBGLastNameMethod(mView: View) {
        mView.requestFocus()
        binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.firstNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.lastNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.emailRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.passwordRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.confirmPasswordRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.genderRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.mobileRL.setBackgroundResource(R.drawable.bg_outline_white)

        if (mView == binding!!.lastNameTV) {
            binding!!.lastNameRL.setBackgroundResource(R.drawable.bg_outline_yellow)
            binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_white)

        }
    }

    private fun changeBGBirthMethod(mView: View) {
        mView.requestFocus()
        binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.firstNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.lastNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.emailRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.passwordRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.confirmPasswordRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.genderRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.mobileRL.setBackgroundResource(R.drawable.bg_outline_white)

        if (mView == binding!!.birthDateTV) {
            binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_yellow)
            datePicker(binding?.birthDateTV!!)
        }

    }


    private fun changeBGGenderMethod(mView: View) {
        mView.requestFocus()
        binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.genderRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.firstNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.lastNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.emailRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.passwordRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.confirmPasswordRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.mobileRL.setBackgroundResource(R.drawable.bg_outline_white)

       if (mView == binding!!.spinnerGender) {
            binding!!.genderRL.setBackgroundResource(R.drawable.bg_outline_yellow)
        }
    }


    private fun changeBGEmailMethod(mView: View) {
        mView.requestFocus()
        binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.genderRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.firstNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.lastNameRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.emailRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.passwordRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.confirmPasswordRL.setBackgroundResource(R.drawable.bg_outline_white)
        binding!!.mobileRL.setBackgroundResource(R.drawable.bg_outline_white)

        if (mView == binding!!.emailTV) {
            binding!!.emailRL.setBackgroundResource(R.drawable.bg_outline_yellow)
            binding!!.genderRL.setBackgroundResource(R.drawable.bg_outline_white)

        }
    }

    // your birthday set bg gray
    override fun editTextSelector(mEditText: View, rl: RelativeLayout, tag: String) {
        mEditText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            binding!!.birthDateRL.setBackgroundResource(R.drawable.bg_outline_white)
            binding!!.genderRL.setBackgroundResource(R.drawable.bg_outline_white)
            binding!!.emailRL.setBackgroundResource(R.drawable.bg_outline_white)
            binding!!.lastNameRL.setBackgroundResource(R.drawable.bg_outline_white)
            if (hasFocus) {
                rl.setBackgroundResource(R.drawable.bg_outline_yellow)
            } else {
                rl.setBackgroundResource(R.drawable.bg_outline_white)
            }
        }
    }



    fun backgroundSelector() {
        editTextSelector(binding?.firstNameET!!, binding!!.firstNameRL, "")
        editTextSelector(binding!!.lastNameET, binding!!.lastNameRL, "")
        editTextSelector(binding!!.emailET, binding!!.emailRL, "")
        editTextSelector(binding!!.mobileET, binding!!.mobileRL, "")
        editTextSelector(binding!!.passwordET, binding!!.passwordRL, "")
        editTextSelector(binding!!.confirmPasswordET, binding!!.confirmPasswordRL, "")
    }


    // set spinner
    private fun setSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()
        mSpinnerList.add("Gender (Optional)")
        mSpinnerList.add("Male")
        mSpinnerList.add("Female")
        mSpinnerList.add("Other")

        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.spinnerGender.adapter = adapter

        binding.spinnerGender.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    backgroundSelector()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    backgroundSelector()
                }
            }

    }

    /*
* Set up validations for Sign In fields
* */
    private fun isValidate(): Boolean {
        var flag = true
        if (binding.firstNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (binding.lastNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (binding.emailET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.text.toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (binding.mobileET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_mobile))
            flag = false
        } else if (binding?.passwordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        } else if (binding?.passwordET?.getText().toString().trim().length < 8) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit))
            flag = false
        } else if (binding?.confirmPasswordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password))
            flag = false
        } else if (binding?.confirmPasswordET?.getText().toString()
                .trim() != binding?.confirmPasswordET?.getText().toString().trim()
        ) {
            showAlertDialog(mActivity, getString(R.string.please_enter_does_not))
            flag = false
        }
        return flag
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!

                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .into(binding.profileIV)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }

            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }


}