package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityMapImageBinding

class MapImageActivity : BaseActivity() {
    lateinit var binding: ActivityMapImageBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapImageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setStatusBar(mActivity, getColor(R.color.home_color))
        }

        performCliks()
    }

    fun performCliks() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }
    }
}