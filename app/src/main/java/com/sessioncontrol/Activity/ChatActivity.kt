package com.sessioncontrol.Activity

import android.os.Build
import android.os.Bundle
import com.sessioncontrol.Adapter.ChatAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityChatBinding

class ChatActivity : BaseActivity() {
    lateinit var binding: ActivityChatBinding
    var chatadapter: ChatAdapter? = null

     var data: MutableList<MessageItemUi> = arrayListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setStatusBar(mActivity, getColor(R.color.home_color)) }

        //click
        performCliks()

        //setAdapter
        setAdapter()

        //add data in list
        addData()
    }

    fun performCliks() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setAdapter() {

        chatadapter = ChatAdapter((mActivity),data)
        binding.chatSendReceRV.adapter=chatadapter

    }

    private fun addData(){
        data.add(MessageItemUi("There are many variations of passages of Lorem",R.color.black,0))
        data.add(MessageItemUi("There are many variations of passages of Lorem",R.color.black,1))
        data.add(MessageItemUi("There are many variations of passages of Lorem",R.color.black,0))
        data.add(MessageItemUi("There are many variations of passages of Lorem",R.color.black,1))

    }
}