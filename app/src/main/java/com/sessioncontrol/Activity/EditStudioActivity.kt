package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.sessioncontrol.Adapter.EditImageAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityEditStudioBinding
import java.io.ByteArrayOutputStream
import java.io.IOException

class EditStudioActivity : BaseActivity() {
    lateinit var binding: ActivityEditStudioBinding
    var editImageAdapter: EditImageAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditStudioBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)


        // perform click
        performClicks()

        //set adapter
        setAdapter()

        //set spinner
        setGenresSpinner()
        setmixingSpinner()
        setPriceSpinner()
    }


    fun performClicks() {

        binding.backRL.setOnClickListener {
            onBackPressed()
        }

        binding.btUpdateTV.setOnClickListener {
            if (isValidate()) {
                onBackPressed()
            }
        }


        binding.ccp.setOnCountryChangeListener {
            binding.codeTV.text = binding.ccp.selectedCountryCodeWithPlus

        }

        binding.AddressTV.setOnClickListener {
            val intent = Intent(this, AddressAccountInfoActivity::class.java)
            startActivity(intent)
        }

        binding.camIV.setOnClickListener(View.OnClickListener {
            openCameraGalleryBottomSheet()
        })

        binding.uploadIV.setOnClickListener(View.OnClickListener {
            openCameraGalleryBottomSheet()

        })
    }


    private fun setAdapter() {
        editImageAdapter = EditImageAdapter(this)
        binding.imagesUploadRV?.adapter = editImageAdapter
    }


    // set spinner
    private fun setGenresSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()
        mSpinnerList.add("Rock")
        mSpinnerList.add("Pop")
        mSpinnerList.add("Pop")
        mSpinnerList.add("Other")


        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.spinnerGenres.adapter = adapter

        binding.spinnerGenres.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    // backgroundSelector()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    //backgroundSelector()
                }
            }

    }

    // set spinner
    private fun setmixingSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()
        mSpinnerList.add("Rock")
        mSpinnerList.add("Pop")
        mSpinnerList.add("Other")


        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.spinnerMastering.adapter = adapter

        binding.spinnerMastering.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    // backgroundSelector()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    // backgroundSelector()
                }
            }

    }

    // set spinner
    private fun setPriceSpinner() {
        val mSpinnerList: ArrayList<String> = ArrayList()
        mSpinnerList.add("10")
        mSpinnerList.add("20")
        mSpinnerList.add("30")


        val adapter = mActivity?.let {
            ArrayAdapter(
                it,
                R.layout.item_spinner,
                R.id.tvSpinner,
                mSpinnerList
            )
        }

        binding.spinnerPrice.adapter = adapter

        binding.spinnerPrice.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    // backgroundSelector()
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    // backgroundSelector()
                }
            }

    }

    /*
* Set up validations for Sign In fields
* */
    fun isValidate(): Boolean {
        val spinnerGen: String = binding.spinnerGenres.getSelectedItem().toString()
        val spinnerMix: String = binding.spinnerMastering.getSelectedItem().toString()
        val spinnerPrice: String = binding.spinnerPrice.getSelectedItem().toString()

        var flag = true
        if (binding.studioNameET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_studio_name))
            flag = false
        } /*else if (binding.AddressTV.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_select_address))
            flag = false
        }*/ else if (binding.emailET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.text.toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (binding.mobileET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_mobile))
            flag = false
        } else if (binding.studioCreditET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_studio_credit))
            flag = false
        } else if (spinnerGen.equals("Rock")) {
            showAlertDialog(mActivity, getString(R.string.please_select_gen))
            flag = false
        } else if (spinnerMix.equals("Rock")) {
            showAlertDialog(mActivity, getString(R.string.please_select_mix))
            flag = false
        } else if (binding.equipmentET.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_studio_equipment))
            flag = false
        } else if (spinnerPrice.equals("10")) {
            showAlertDialog(mActivity, getString(R.string.please_select_price))
            flag = false
        }
        return flag
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!

                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .into(binding.profileIV)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }

            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }

}