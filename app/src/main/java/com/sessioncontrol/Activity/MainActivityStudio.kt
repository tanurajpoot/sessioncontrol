package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Bundle
import com.google.android.material.navigation.NavigationBarView
import com.sessioncontrol.Fragment.*
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityMainStudioBinding

class MainActivityStudio : BaseActivity() {
    private lateinit var binding: ActivityMainStudioBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainStudioBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        switchFragment(MusicStudioFragment(), false, null)

        binding.navigationId.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener {

            when (it.itemId) {

                R.id.actionMusic -> {
                    switchFragment(MusicStudioFragment(), false, null)
                    false
                }
                R.id.actionCalender -> {
                    switchFragment(CalnderFragment(), false, null)
                    false
                }

                R.id.actionMessage -> {
                    switchFragment(MessageFragment(), false, null)
                    false
                }
                R.id.actionNotification -> {
                    switchFragment(NotificationFragment(), false, null)
                    false
                }
                R.id.actionProfile -> {
                    switchFragment(ProfileStudioFragment(), false, null)
                    false
                }
            }
            true
        })
    }

    override fun onBackPressed() {
        finishAffinity()
    }

}