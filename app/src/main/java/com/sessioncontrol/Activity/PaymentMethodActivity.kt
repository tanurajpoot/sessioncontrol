package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.sessioncontrol.databinding.ActivityPaymentMethodBinding


class PaymentMethodActivity : BaseActivity() {

    lateinit var binding: ActivityPaymentMethodBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentMethodBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()
    }


    fun performClick() {

        binding.backRL.setOnClickListener(View.OnClickListener {
                onBackPressed()

        })

        binding.addRL.setOnClickListener(View.OnClickListener {
            val intent = Intent(this,AddPaymentMethodActivity::class.java)
            startActivity(intent)
        })
    }
}