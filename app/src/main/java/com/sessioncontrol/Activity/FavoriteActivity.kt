package com.sessioncontrol.Activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityAddPaymentMethodBinding
import com.sessioncontrol.databinding.ActivityFavoriteBinding

class FavoriteActivity : BaseActivity() {
    lateinit var binding: ActivityFavoriteBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFavoriteBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()
    }

    fun performClick() {

        binding.backRL.setOnClickListener {
            onBackPressed()

        }
    }
}