package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Bundle
import com.sessioncontrol.Adapter.AddressAdapter
import com.sessioncontrol.Adapter.NotificationAdapter
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityAddressAccountInfoBinding

class AddressAccountInfoActivity : BaseActivity() {

    lateinit var binding: ActivityAddressAccountInfoBinding
    var addressAdapter: AddressAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddressAccountInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //adapter
        setAdapter()

        //clicks
        performCliks()
    }


    fun performCliks() {
        binding.backRL.setOnClickListener {
            onBackPressed()
        }

    }

    private fun setAdapter() {
        addressAdapter = AddressAdapter(this)
        binding.notificationRV?.adapter = addressAdapter
    }
}