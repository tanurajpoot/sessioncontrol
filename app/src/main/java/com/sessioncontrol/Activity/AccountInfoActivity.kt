package com.sessioncontrol.Activity

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivityAccountInfoBinding

class AccountInfoActivity : BaseActivity() {
    lateinit var binding: ActivityAccountInfoBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity,Color.BLACK)

        //clicks
        performClick()

    }

    fun performClick() {
        binding.backRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }
}