package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import com.sessioncontrol.MainActivity
import com.sessioncontrol.R
import com.sessioncontrol.databinding.ActivitySplashScreenBinding

class SplashScreenActivity : BaseActivity() {
    lateinit var binding: ActivitySplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

       // setUpSplash()
        performClick()
    }


    fun performClick() {

        binding.btSignInTV.setOnClickListener(View.OnClickListener {

                var intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
        })

        binding.btSignUPNowTV.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
            finish()

        })

    }

/*    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(3000)
                val i = Intent(this@SplashScreenActivity, LoginActivity::class.java)
                startActivity(i)
                finish()

            }
        }
        mThread.start()
    }*/
}