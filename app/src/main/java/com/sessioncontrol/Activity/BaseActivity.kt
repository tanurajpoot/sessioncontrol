package com.sessioncontrol.Activity

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.bumptech.glide.Glide
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.sessioncontrol.R
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

open class BaseActivity : AppCompatActivity() {

    lateinit var mBitmap: Bitmap
     var mCheckCamGal: String = ""


    open var TAG: String = this@BaseActivity.javaClass.getSimpleName()
    open var mActivity: Activity = this@BaseActivity

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = Manifest.permission.CAMERA

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun setStatusBar(mActivity: Activity?, mColor: Int) {
        window.statusBarColor = mColor
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }


    // alert dialog pop
    open fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialoge_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    //Validate Email Address
    open fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }


    //to switch between fragments*
    open fun switchFragment(
        fragment: Fragment?,
        //Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager: FragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayout, fragment).addToBackStack(null)
            fragmentManager.popBackStack()

//            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            //  fragmentManager.executePendingTransactions()
        }


    }


    // - - Sets Appointment Date
    open fun datePicker(mTextView: TextView) {

        val text: TextView? = null
        val sdf = SimpleDateFormat("MM/dd/yy", Locale.getDefault())
        val mycalender = Calendar.getInstance()
        val date =
            DatePickerDialog.OnDateSetListener { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
                mycalender[Calendar.YEAR] = year
                mycalender[Calendar.MONTH] = month
                mycalender[Calendar.DAY_OF_MONTH] = dayOfMonth
                mTextView.setText(sdf.format(mycalender.time))
            }
        mTextView.setOnClickListener(View.OnClickListener { view: View? ->
            DatePickerDialog(
                this, date,

                mycalender[Calendar.YEAR],
                mycalender[Calendar.MONTH],
                mycalender[Calendar.DAY_OF_MONTH]
            ).show()
        })
    }


    // edit text focus color chnage
    open fun editTextSelector(mEditText: EditText, rl: RelativeLayout, tag: String) {

        // rl.setBackgroundResource(R.drawable.bg_outline_gray)
        mEditText.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
            if (b) {
                rl.setBackgroundResource(R.drawable.bg_outline_yellow)
            } else {
                rl.setBackgroundResource(R.drawable.bg_outline_white)
            }

        }
    }

    /*  // - - To Show or Hide Password
    open fun setPasswordHideShow(editText: EditText, imageView: ImageView) {
        if (editText.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
            imageView.setImageResource(R.drawable.ic_eye_hide)
            editText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editText.setSelection(editText.text.toString().trim().length)
        } else {
            imageView.setImageResource(R.drawable.ic_eye_show);
            editText.transformationMethod = PasswordTransformationMethod.getInstance()
            editText.setSelection(editText.text.toString().trim().length)
        }
    }*/

    open fun backArrow(relativeLayout: RelativeLayout) {
        relativeLayout.setOnClickListener {
            onBackPressed()
        }

    }

    open fun editTextSelector(mEditText: View, rl: RelativeLayout, tag: String) {}


    fun openCameraGalleryBottomSheet() {

        val dialog = BottomSheetDialog(this)
        val view: View =
            LayoutInflater.from(this).inflate(R.layout.bottomsheet_carema, null)
        dialog!!.setContentView(view)

        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)

        var takePhotoTv = view.findViewById<TextView>(R.id.takePhotoTV)
      //  var caremaTV = view.findViewById<TextView>(R.id.cameraTv)
        var PhotoLibraryTV = view.findViewById<TextView>(R.id.photoLibraryTV)
        var btCancelTV = view.findViewById<TextView>(R.id.btCancelTV)

        takePhotoTv?.setOnClickListener(View.OnClickListener {

            mCheckCamGal = "camera"
            if (checkPermission()) {
                onSelectCameraClick()
            } else {
                requestPermission()
            }
            dialog.dismiss()
        })

     /*   caremaTV?.setOnClickListener(View.OnClickListener {
            mCheckCamGal = "camera"
            if (checkPermission()) {
                onSelectCameraClick()
            } else {
                requestPermission()
            }
            dialog.dismiss()
        })*/


        PhotoLibraryTV?.setOnClickListener(View.OnClickListener {
            mCheckCamGal = "gallery"
            if (checkPermission()) {
                onSelectImageClick()
            } else {
                requestPermission()
            }
            dialog.dismiss()
        })

        btCancelTV.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })


        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()

    }

    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {

                    if (mCheckCamGal.equals("gallery")){
                        onSelectImageClick()

                    } else if (mCheckCamGal.equals("camera")){
                        onSelectCameraClick()
                    }
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            //Crop image(Optional), Check Customization for more option
            .crop()
            .galleryOnly()

            //Final image size will be less than 1 MB(Optional)
            .compress(120)

            //Final image resolution will be less than 1080 x 1080(Optional)
            .maxResultSize(200, 200)
            .cropSquare()
            .start()
    }


    // - - Start Image Picker Activity
    public fun onSelectCameraClick() {
        ImagePicker.with(this)
            //Crop image(Optional), Check Customization for more option
            .crop()
            .cameraOnly()

            //Final image size will be less than 1 MB(Optional)
            .compress(120)

            //Final image resolution will be less than 1080 x 1080(Optional)
            .maxResultSize(200, 200)
            .cropSquare()
            .start()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!

                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                       // .into(binding.addCameraGallery)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }

            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }


}