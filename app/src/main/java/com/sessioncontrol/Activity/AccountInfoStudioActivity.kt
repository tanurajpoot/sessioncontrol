package com.sessioncontrol.Activity

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.sessioncontrol.databinding.ActivityAccountInfoStudioBinding


class AccountInfoStudioActivity : BaseActivity() {
    lateinit var binding: ActivityAccountInfoStudioBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountInfoStudioBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.BLACK)

        //clicks
        performClick()
    }


    fun performClick() {
        binding.backRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }

}